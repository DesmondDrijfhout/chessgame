package src.model;

public class SchaakStuk {
    private int locationX;
    private int locationY;
    private boolean isWhite;

    public int getLocationX() {
        return locationX;
    }

    public void setLocationX(int locationX) {
        this.locationX = locationX;
    }

    public int getLocationY() {
        return locationY;
    }

    public void setLocationY(int locationY) {
        this.locationY = locationY;
    }

    public boolean isIswhite() {
        return isWhite;
    }

    public void setIswhite(boolean iswhite) {
        this.isWhite = iswhite;
    }
}
